var express = require('express');
var router = express.Router();
var query = require('../db/query');

/* GET home page. */
router.get('/', function(req, res, next) {
  query.bookdetails(function(err, results){
    // console.log(err || results)
    res.json(results);
  })

  
});

module.exports = router;