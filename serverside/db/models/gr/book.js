const mongoose = require('mongoose');

let bookSchema = new mongoose.Schema({
    bookTitle: String,         // #bookTitle
    bookUrl: String,
    bookImageUrl: String,
    bookLargeImageUrl: String, // #boxContents img
    authorName: String,        // #aboutAuthor div.bookAuthorProfile__name > a - value
    authorUrl: String,         // #aboutAuthor div.bookAuthorProfile__name > a - href
    rating: Number,            // #bookMeta > span.value.rating
    ratingCount: Number,       // #bookMeta span.votes
    reviewCount : Number,      // #bookMeta span.count
    genres : [String],           // a.bookPageGenreLink[href*="genres"]
    genreUrl: String,
    paperback: Boolean,
    hardcover: Boolean,
    pageCount: Number,
    details : String,          // #details div.row
    publisherName: String,
    dateCrawled: Date,
    isbn : Number,
    isbn13: Number,            // #bookDataBox span[itemprop*="isbn"]
    asin: String,
    language: String,
    editionsCount: Number,
    editions: Array,
    kindleLink: String,
    kindlePrice: String,       // #buyButtonContainer a.glideButton - extract value
    description: String,
    parsed : Number,
});

let Book = mongoose.model('Book', bookSchema);

module.exports = Book;