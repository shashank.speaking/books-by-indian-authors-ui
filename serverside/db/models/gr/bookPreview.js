const mongoose = require('mongoose');

let bookPreviewSchema = new mongoose.Schema({
    bookTitle: String,
    bookUrl: String,
    bookImageUrl: String,
    authorName: String,
    authorUrl: String,
    rating: Number,
    ratingCount: Number,
    publishedYear : Number,
    shelfUrl: String,
    shelfName: String,
    details: String
});

let BookPreview = mongoose.model('BookPreview', bookPreviewSchema);

module.exports = BookPreview;