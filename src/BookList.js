import React, { Component } from 'react';
import {Tooltip, OverlayTrigger} from 'react-bootstrap';
import url from 'url';
import BookPreview from './BookPreview';

class BookList extends Component {

  state = {
    response: []
  };

  componentDidMount() {
    this.callApi()
      .then(res => {
        console.log (res); 
        this.setState({ response: res })
      })
      .catch(err => console.log(err));
  }

  callApi = async () => {
    const response = await fetch('/api/bookdetails');
    const body = await response.json();

    if (response.status !== 200) throw Error(body.message);

    return body;
  };

  render() {
    let numbers = this.state.response
    

    let listItems = numbers.map((book) => {
      // <li>{book.bookTitle}</li>
    let LARGE = "l";
    let MED = "m";
    let imageUrl = book.bookImageUrl;
    if(!imageUrl || imageUrl.includes("nophoto")){

      return (
        <div className="cover-outer" data-title={book.bookTitle + " - " + book.authorName}> 
          <a href={book.bookUrl}> 
            <img 
              width="140px" 
              height="200px" 
              style={{'paddingLeft': '10px', 'paddingBottom': '10px'}} 
              src="https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png"
              title={book.bookTitle + " - " + book.authorName}/>
            <span>{book.bookTitle + " - " + book.authorName}</span>
          </a>
        </div>
      )
    }

    if(imageUrl && !imageUrl.includes("nophoto")){
      let parsedUrl = url.parse(imageUrl);
    let path = parsedUrl.pathname;
    let parts = path.split('/')
    let imagename = parts[parts.length - 2]
    imagename = imagename.substring(0, imagename.length - 1) + LARGE;
    parts[parts.length - 2] = imagename
    parsedUrl.pathname = parts.join("/")
    book.imageHref = parsedUrl.format();
    }else{
      book.imageHref = imageUrl
    }
    
    const tooltip = (
      <Tooltip id="tooltip">
        {/* <strong>Holy guacamole!</strong> Check this info. */}
        <BookPreview book={book}> </BookPreview>
      </Tooltip>
    );
      return (
        <OverlayTrigger defaultOverlayShown={/* book.bookTitle=="Mahashweta"?true:false */ false} placement="bottom" overlay={tooltip}>
          <div> <a href={book.bookUrl}> <img width="140px" height="200px" style={{'paddingLeft': '10px', 'paddingBottom': '10px'}} src={book.imageHref} title={book.bookTitle + " - " + book.authorName}/></a> </div>
        </OverlayTrigger>
      )
    });
    return (
      <ul>{listItems}</ul>
    );
  }
  
}
export default BookList;
