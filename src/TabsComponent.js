import { Tabs, TabList, Tab, TabPanel } from 'react-tabs';
import React, { Component } from 'react';
import BookList from './BookList'
import ReactTableComponent from './ReactTableComponent'

class TabsComponent extends Component {
    constructor() {
      super();
      this.state = { tabIndex: 0 };
    }
    render() {
      return (
        <Tabs selectedIndex={this.state.tabIndex} onSelect={tabIndex => this.setState({ tabIndex })}>
          <TabList>
            <Tab><h4>Covers</h4></Tab>
            <Tab><h4>Details</h4></Tab>
          </TabList>
          <TabPanel>
            <div className="booklist">
                <BookList> </BookList>
            </div>
          </TabPanel>
          <TabPanel>
          <ReactTableComponent></ReactTableComponent>
          </TabPanel>
        </Tabs>
      );
    }
  }

  export default TabsComponent;