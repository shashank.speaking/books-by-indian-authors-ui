import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import BookList from './BookList'
import TabsComponent from './TabsComponent'
import 'react-tabs/style/react-tabs.css';


class App extends Component {

  
  render() {
    return (
      <div className="App">
        <header className="App-header">
          {/* <img src={logo} className="App-logo" alt="logo" /> */}
          <h1 className="App-title">Read Indian.com <p/> Books by <i> Indian </i> Authors</h1>
          
        </header>
        {/* <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p> */}
        {/* <span>{this.state.response}</span> */}
        <TabsComponent></TabsComponent>
        
      </div>
    );
  }
}

export default App;
