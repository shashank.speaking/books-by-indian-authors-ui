import React, { Component } from 'react';
import url from 'url';

class BookTable extends Component {

  state = {
    response: []
  };

  componentDidMount() {
    this.callApi()
      .then(res => {
        console.log (res); 
        this.setState({ response: res })
      })
      .catch(err => console.log(err));
  }

  callApi = async () => {
    const response = await fetch('/api/book');
    const body = await response.json();

    if (response.status !== 200) throw Error(body.message);

    return body;
  };

  render() {
    let numbers = this.state.response
    

    let listItems = numbers.map((book) => {
      // <li>{book.bookTitle}</li>
    let LARGE = "l";
    let MED = "m";
    let imageUrl = book.bookImageUrl;
    if(!imageUrl || imageUrl.includes("nophoto")){

      return( 
        <div>
        <div className="outer"
          title={book.bookTitle}
          style={{}}>
        <a className="bottomleft"
           href={book.bookUrl}
            style={{}}>
              <p>{book.bookTitle + "\n" + book.authorName}</p>
        </a>
        
      </div>
      </div>)

      // return ( 
      //   <div>
      //     <a > 
      //       <img 
      //         width="140px" 
      //         height="200px" 
      //         style={{'paddingLeft': '10px', 'paddingBottom': '10px'}} 
      //         src={"https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png"} 
      //         title={book.bookTitle + " - " + book.authorName}/>
      //         </a>
      //         <span
      //         title={book.bookTitle + " - " + book.authorName}
      //         style={{
      //           position: "absolute",
      //           top:"0px",
      //           // backgroundImage : "url(https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png)",
      //           maxWidth:"140px",
      //           maxHeight:"200px",
      //           textOverflow:"ellipsis"
      //         }}
      //         >
      //         {book.bookTitle + "\n" + book.authorName}
      //         </span>
            
      //     </div>)
    }

    if(imageUrl && !imageUrl.includes("nophoto")){
      let parsedUrl = url.parse(imageUrl);
    let path = parsedUrl.pathname;
    let parts = path.split('/')
    let imagename = parts[parts.length - 2]
    imagename = imagename.substring(0, imagename.length - 1) + LARGE;
    parts[parts.length - 2] = imagename
    parsedUrl.pathname = parts.join("/")
    book.imageHref = parsedUrl.format();
    }else{
      book.imageHref = imageUrl
    }
    

      return <div> <a href={book.bookUrl}> <img width="140px" height="200px" style={{'paddingLeft': '10px', 'paddingBottom': '10px'}} src={book.imageHref} title={book.bookTitle + " - " + book.authorName}/></a> </div>
    });
    return (
      <ul>{listItems}</ul>
    );
  }
  
}
export default BookTable;
