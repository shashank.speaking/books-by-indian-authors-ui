import React, { Component } from 'react';
import url from 'url';

class BookPreview extends Component {

  state = {
    response: []
  };

  toLargeImageUrl(imageUrl){
    let LARGE = "l";
    let parsedUrl = url.parse(imageUrl);
    let path = parsedUrl.pathname;
    let parts = path.split('/')
    let imagename = parts[parts.length - 2]
    imagename = imagename.substring(0, imagename.length - 1) + LARGE;
    parts[parts.length - 2] = imagename
    parsedUrl.pathname = parts.join("/")
    return parsedUrl.format();
  }

  render() {
    
      // <li>{book.bookTitle}</li>
      let book = this.props.book;
      let imageUrl = book.bookImageUrl;
      if(!imageUrl || imageUrl.includes("nophoto")){
  
        return (
          <div className="cover-outer" data-title={book.bookTitle + " - " + book.authorName}> 
            <a href={book.bookUrl}> 
              <img 
                width="140px" 
                height="200px" 
                style={{'paddingLeft': '10px', 'paddingBottom': '10px'}} 
                src="https://s.gr-assets.com/assets/nophoto/book/50x75-a91bf249278a81aabab721ef782c4a74.png"
                title={book.bookTitle + " - " + book.authorName}/>
              <span>{book.bookTitle + " - " + book.authorName}</span>
            </a>
          </div>
        )
      }
  
      // convert small image url to large image url
      
      book.imageHref = 
        (imageUrl && !imageUrl.includes("nophoto"))?this.toLargeImageUrl(imageUrl):imageUrl;
      
      return <div className={"overlay content"}> 
        <h3  className="title"> {book.bookTitle + " - " + book.authorName} </h3>
        <a href={book.bookUrl}> 
          <img width="140px" height="200px" style={{'paddingLeft': '10px', 'paddingBottom': '10px'}} src={book.imageHref} title={book.bookTitle + " - " + book.authorName}/>
        </a>
        <span className="rating">{book.rating}/5</span><br/>
        <span className="description">{book.description}</span>
        </div>
      
  }
  
}
export default BookPreview;
