import React from "react";
import { render } from "react-dom";
import { Grid, Row, Col, Label } from 'react-bootstrap';
// import { makeData, Logo, Tips } from "./Utils";

// Import React Table
import ReactTable from "react-table";
import "react-table/react-table.css";
import {tworows} from "./mocks/table.mock";

class ReactTableComponent extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [
        {firstName: "A", lastName: "B" , age : "1", status: "S", visits: "4"}
      ],
      mockdata : tworows,
      response : []
    };
  }

  componentDidMount() {
    this.callApi()
      .then(res => {
        console.log (res); 
        this.setState({ response: res })
      })
      .catch(err => console.log(err));
  }

  callApi = async () => {
    const response = await fetch('/api/bookdetails');
    const body = await response.json();

    if (response.status !== 200) throw Error(body.message);

    return body;
  };

  render() {
    const { data } = this.state;
    return (
      <div>
        <ReactTable
          data={this.state.response}
          SubComponent={row => {
          return ( 
            <Grid fluid={true} className="subcomponent">
            <Row>
                <Col xs={12}>
                    {row.row.genres.map( x => <Label bsStyle="primary">{x}</Label>)} 
                </Col>
            </Row>
            <Row>
                <Col xs={12}>
                    {row.row.description}
                </Col>
            </Row>
            </Grid>
  
            // <div className="row subcompoent">
            //     <span style={{ paddingLeft: "20px" }}> {row.row.description}</span> 
            // </div>
        )
          }}
          columns={[
                {
              
                  Header: "Author",
                  id : "authorName",
                  accessor: "authorName",
                  Cell : (props) => {
                      return (
                      <a href={props.row.authorUrl}>{props.row.authorName}</a>
                    ) 
                  }
                },
                {
                  Header: "Url",
                  id: "authorUrl",
                  show: false,
                  accessor: d => d.authorUrl
                },
              
                {
                  Header: "Book Title",
                  accessor: "bookTitle",
                  Cell : (props) => {
                    return (
                    <a href={props.row.bookUrl}>{props.row.bookTitle}</a>
                    )
                  }
                },
                {
                    Header: "Book Cover",
                    id: "bookImageUrl",
                    accessor: d => d.bookImageUrl,
                    Cell : (props) => {
                        return (
                        <img src={props.row.bookImageUrl}/>
                        )
                      },
                    filterable: false
                },
                {
                    Header: "Book Url",
                    id: "bookUrl",
                    show: false,
                    accessor: d => d.bookUrl
                },
                {
                    Header: "Rating (out of 5)",
                    accessor: "rating",
                    defaultSortDesc : true,
                },
                {
                    Header: "No. of votes",
                    accessor: "ratingCount"
                },
                {
                    Header: "No. of Reviews",
                    accessor: "reviewCount"
                },
                {
                    Header: "Kindle Price",
                    accessor: "kindlePrice"
                },
                {
                    Header: "Description",
                    accessor: "description",
                    show : false,
                },
                {
                    Header: "Genres",
                    accessor: "genres",
                    show : false, 
                },
                {
                    Header: "Details",
                    accessor: "details",
                    show : false,
                }
              
            
          ]}
          defaultFilterMethod = {(filter, row, column) => {
            const id = filter.pivotId || filter.id
            return row[id] !== undefined ? String(row[id]).toLowerCase().includes(filter.value.toLowerCase()) : true
          }}
          defaultPageSize={10}
          className="-striped -highlight"
          defaultPageSize = {100}
          filterable
        />
      </div>
    );
  }
}

export default ReactTableComponent;
